// Import express
const express = require("express");
const session = require("express-session");
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const fs = require("fs");
const app = express();

// Add functionalities to the express
// For server side rendering of the pages
app.use(express.static("public"));
app.set("views", `${__dirname}/view`);
app.set("view engine", "pug");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json()); // used to parse json files to http body
app.use(fileUpload()); // used to parse json files to http body
app.use(session({ secret: "secret", resave: true, saveUninitialized: true, cookie: { secure: false } })); //cookies to prevent user from accessing admin url w/o pass

// Create Connection to the database
const admindb = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "bavab",
});

// Connect the admin db to the database
admindb.connect((err) => {
  if (err) {
    console.log("There is something wrong in connecting to the database");
    console.log(err);
  } else {
    console.log("Server Connected.");
  }
});

// PORT CONSTANT
const PORT = process.env.PORT || 8001;

// start server listen
app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
});

// PAGES
/* ------------ PUBLIC FILES ------------------ */
app.get("/", (request, response) => {
  return response.sendFile(path.join(`${__dirname}/view/index.html`));
});

app.get("/players", (request, response) => {
  return response.sendFile(path.join(`${__dirname}/view/players.html`));
});

app.get("/seasons", (request, response) => {
  return response.sendFile(path.join(`${__dirname}/view/seasons.html`));
});

app.get("/teams", (request, response) => {
  return response.sendFile(path.join(`${__dirname}/view/teams.html`));
});
/* ------------ END OF PUBLIC FILES ------------------ */

/* ---------------- ADMIN PAGES  ---------------------------*/
app.get("/admin", (request, response) => {
  console.log(`CURRENT USER: ${request.session.admin}`);
  if (checkSession(request)) {
    return response.redirect("/admin/home");
  } else {
    return response.render("./admin-pages/login");
  }
});

app.get("/admin/home", (request, response) => {
  if (checkSession(request)) {
    var user = { username: request.session.admin };
    return response.render("./admin-pages/admin", { user });
    // return response.render("./admin-v2/admin");
  } else {
    return response.redirect("/admin");
  }
});

// Render Players Page
app.get("/admin/players", async (request, response) => {
  console.log("Players Page");
  var user = { username: request.session.admin };
  var page = request.query.page;
  var perPage = 40;
  var totalRows = await getTotalRows("players");
  var searchKey = request.query.search;
  if (page === undefined) {
    page = 0;
  }
  if (searchKey === undefined) {
    searchKey = "";
  }
  meta = {
    totalCount: totalRows,
    totalPages: parseInt(totalRows / parseInt(perPage)) + 1,
    currentPage: page,
    search: searchKey,
  };
  if (checkSession(request)) {
    admindb.query(
      `SELECT playerId, strPlayerName, intHeight, intWeight, strTeam FROM players INNER JOIN teams ON teams.teamId = players.teamId WHERE players.strPlayerName LIKE "%${searchKey}%" ORDER BY players.playerId LIMIT ? OFFSET ?`,
      [perPage, page * perPage],
      async (error, result, fields) => {
        if (result) {
          var playerList = [];
          // GET ALL TEAM NAME
          const teamNameList = [];
          await getTeamList().then((results) => {
            results.forEach((result) => {
              teamNameList.push({ strTeam: result.strTeam, teamId: result.teamId });
            });
          });
          for (let i = 0; i < result.length; i++) {
            playerList.push({
              playerId: result[i].playerId,
              playerName: result[i].strPlayerName,
              playerWeight: result[i].intWeight,
              playerHeight: result[i].intHeight,
              playerTeam: result[i].strTeam,
            });
          }
          // console.log(playersList);
          return response.render("./admin-pages/admin-players", { playerList, teamNameList: teamNameList, user, meta });
        } else {
          console.log(error);
        }
      }
    );
  } else {
    return response.redirect("/admin");
  }
});

app.get("/admin/events", async (request, response) => {
  console.log("Events Page");
  var page = request.query.page;
  var search = request.query.search;
  var season = request.query.season;
  var page = request.query.page;
  var perPage = 20;
  var totalRows = await getTotalRows("events");
  // console.log(page);
  // console.log(search);
  // console.log(season);
  var user = { username: request.session.admin };
  meta = {
    totalCount: totalRows,
    totalPages: parseInt(totalRows / parseInt(perPage)) + 1,
    currentPage: page,
  };

  if (page === undefined) {
    page = 0;
  }

  if (checkSession(request)) {
    admindb.query(
      "SELECT eventId, season, intHomeScore, intAwayScore, dateEvent, strVideo, homeTeam.strTeam as strHomeTeam, awayTeam.strTeam as strAwayTeam FROM events INNER JOIN teams homeTeam ON events.homeId = homeTeam.teamId INNER JOIN teams awayTeam ON events.awayId = awayTeam.teamId ORDER BY eventId LIMIT ? OFFSET ?",
      [perPage, parseInt(page * perPage)],
      async (error, result, fields) => {
        var eventList = [];
        // GET ALL TEAM NAME
        const teamNameList = [];
        await getTeamList().then((results) => {
          results.forEach((result) => {
            teamNameList.push({ strTeam: result.strTeam, teamId: result.teamId });
          });
        });
        for (let i = 0; i < result.length; i++) {
          eventList.push({
            eventId: result[i].eventId,
            season: result[i].season,
            strHomeTeam: result[i].strHomeTeam,
            strAwayTeam: result[i].strAwayTeam,
            intHomeScore: result[i].intHomeScore,
            intAwayScore: result[i].intAwayScore,
            dateEvent: result[i].dateEvent,
            strVideo: result[i].strVideo,
          });
        }
        return response.render("./admin-pages/admin-events", { eventList, teamNameList: teamNameList, user, meta });
      }
    );
  } else {
    return response.redirect("/admin");
  }
});

app.get("/admin/description", (request, response) => {
  console.log("Description Page");
  var user = { username: request.session.admin };
  if (checkSession(request)) {
    admindb.query("SELECT * FROM description", (error, result, fields) => {
      var description = {
        descId: result[0].descId,
        strBadge: result[0].strBadge,
        strDescriptionEN: result[0].strDescriptionEN,
        strWebsite: result[0].strWebsite,
      };
      // console.log(description);
      return response.render("./admin-pages/admin-description", { description, user });
    });
  } else {
    return response.redirect("/admin");
  }
});

app.get("/admin/teams", async (request, response) => {
  console.log("TEAMS Page");
  const user = { username: request.session.user };
  var totalRows = await getTotalRows("teams");
  var meta = {
    totalCount: totalRows,
  };
  if (checkSession(request)) {
    admindb.query("SELECT * FROM teams", async (error, result, fields) => {
      var teamList = [];
      for (let i = 0; i < result.length; i++) {
        teamList.push({
          teamId: result[i].teamId,
          strTeam: result[i].strTeam,
          strTeamShort: result[i].strTeamShort,
          intFormedYear: result[i].intFormedYear,
          strDescriptionEN: result[i].strDescriptionEN,
          strTeamBadge: result[i].strTeamBadge,
        });
      }
      // console.log(teamList);
      return response.render("./admin-pages/admin-teams", { teamList, user, meta });
    });
  } else {
    return response.redirect("/admin");
  }
});

app.get("/admin/error", (req, res) => {
  const error = req.query.error;
  console.log(error);
  return res.render("./admin-pages/error-page", { error });
});

/* ----------------  END OF ADMIN PAGES  ---------------------------*/

/* -------------- START OF ADMIN COMMANDS -------------------------*/

// Admin Login
app.post("/admin/api/login", (req, res) => {
  try {
    // Convert the givenr request to object
    const userObject = { username: req.body.username, password: req.body.password };
    // Search all the object with queries
    admindb.query(
      "SELECT * FROM admin WHERE username = ? and password = ?",
      [userObject.username, userObject.password],
      async (err, result, fields) => {
        // console.log(result);
        // If username password is found
        if (result.length > 0) {
          req.session.admin = userObject.username;
          console.log("Logged In, session saved,redirecting");
          res.redirect("/admin/home");
        } else {
          console.log("Invalid Input");
          res.redirect("/admin");
        }
      }
    );
  } catch (error) {
    return res.status(500).json({ message: "An internal server error occured.", error: error.message });
  }
});

// Admin Logout
app.get("/admin/api/logout", (request, response) => {
  // Destroy the current session
  request.session.destroy();
  console.log("Logging out, destroyed session, redirecting...");
  response.redirect("/admin");
});

// EDIT AND DELETE PLAYER DEPENDING ON THE SUBMIT BUTTON GIVEN
app.post("/admin/api/editPlayer", (request, response) => {
  const pObj = ({ playerId, strPlayerName, intHeight, intWeight, playerTeam } = request.body);
  // IF ALL INPUT ARE PRESENT
  console.log(pObj);
  if (request.body.submitType == "EDIT") {
    console.log("EDITTING PLAYER");
    if (pObj) {
      // TEAM EDIT NOT IMPLEMENTED
      admindb.query(
        "UPDATE players SET strPlayerName = ?, intHeight = ?, intWeight = ?  WHERE playerId = ?",
        [removeTags(pObj.strPlayerName), pObj.intHeight, pObj.intWeight, pObj.playerId],
        (error, result, fields) => {
          if (result) {
            console.log("UPDATE PLAYER COMPLETE");
            return response.redirect("/admin/players");
          } else {
            console.log("An error had occured");
            console.log(error);
            return response.redirect("/admin/players");
          }
        }
      );
    }
  }
  if (request.body.submitType == "DELETE") {
    console.log("DELETING PLAYER");
    admindb.query("DELETE FROM players WHERE playerId = ?", [pObj.playerId], (error, result, fields) => {
      if (result) {
        console.log(`Successfullyl Delelted ID: ${pObj.playerId}`);
        return response.redirect("/admin/players");
      }
    });
  }
});

// ADD PLAYER ONLY
app.post("/admin/api/addPlayer", async (request, response) => {
  try {
    console.log("ADDING PLAYER");
    var playername = request.body.strPlayerName;
    var height = parseInt(request.body.intHeight);
    var weight = parseInt(request.body.intWeight);
    var teamId = parseInt(request.body.teamId);
    var newHighestId = await getHighestId("players", "playerId").then((result) => parseInt(result[0].highest) + 1);
    if (isNaN(newHighestId)) {
      newHighestId = 0;
    }
    admindb.query(
      `INSERT INTO players (playerId, strPlayerName, intHeight, intWeight, teamId) VALUES (?,?,?,?,?)`,
      [newHighestId, removeTags(playername), height, weight, teamId],
      (error, result, fields) => {
        if (result) {
          console.log("Successfully Added!");
        } else {
          console.log("Something Went Wrong!");
          console.log(error);
        }
      }
    );
    return response.redirect("/admin/players");
  } catch (error) {
    console.log(error);
    return response.redirect(`/admin/error?error=${error}`);
  }
});

// EDIT AND DELETE EVENT
app.post("/admin/api/editEvent", (request, response) => {
  try {
    console.log("EDITTING EVENT");
    var season = getSeasonString(request.body.dateEvent);

    // IF THE URL CONTAINS youtube link
    var videoLink = "";
    if (
      /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/g.test(
        request.body.strVideo
      )
    ) {
      videoLink = request.body.strVideo;
    }

    if (request.body.submitType === "EDIT") {
      admindb.query(
        "UPDATE events SET season = ?, intHomeScore = ?, intAwayScore = ?, dateEvent = ?, strVideo = ? WHERE eventId = ?",
        [
          season,
          request.body.intHomeScore,
          request.body.intAwayScore,
          request.body.dateEvent,
          videoLink,
          request.body.eventId,
        ],
        (error, result, fields) => {
          if (result) {
            console.log("SUCCESSFULLY EDITTED");
          } else {
            console.log(error);
          }
        }
      );
    }
    if (request.body.submitType === "DELETE") {
      console.log("DELETING EVENT");
      admindb.query("DELETE FROM events WHERE eventId = ?", [request.body.eventId], (error, result, fields) => {
        if (result) {
          console.log(`Successfullyl Delelted ID: ${request.body.eventId}`);
        } else {
          console.log(error);
        }
      });
    }
    return response.redirect("/admin/events");
  } catch (error) {
    console.log(error);
    return response.redirect(`/admin/error?error=${error}`);
  }
});

// ADD EVENT
app.post("/admin/api/addEvent", async (request, response) => {
  try {
    console.log(request.body);
    var newHighestEventId = await getHighestId("events", "eventId").then((result) => result[0].highest + 1);
    // VERIFY FIRST IF there are highest event id
    if (isNaN(newHighestEventId)) {
      newHighestEventId = 0;
    }
    console.log(newHighestEventId);
    console.log(request.body.dateEvent);

    // IF THE URL CONTAINS youtube link
    var videoLink = "";
    if (
      /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/g.test(
        request.body.strVideo
      )
    ) {
      videoLink = request.body.strVideo;
    }

    // Constructs the season string based on the date given
    var season = getSeasonString(request.body.dateEvent);
    console.log(season);

    if (
      request.body.teamId[0] != request.body.teamId[1] &&
      (request.body.dateEvent != "") & (request.body.season != "")
    ) {
      admindb.query(
        `INSERT INTO events (eventId, season, intHomeScore, intAwayScore, dateEvent, strVideo, homeId, awayId) VALUES (?,?,?,?,?,?,?,?)`,
        [
          newHighestEventId,
          season,
          request.body.intHomeScore,
          request.body.intAwayScore,
          request.body.dateEvent,
          videoLink,
          request.body.teamId[0],
          request.body.teamId[1],
        ],
        (error, result, fields) => {
          if (result) {
            console.log("ADDED EVENT");
            // console.log(result);
          } else {
            console.log("INVALID ADD EVENT FORMAT");
            // console.log(error);
          }
        }
      );
    } else {
      console.log("There is something wrong with your Input!");
      var error = "";
      return response.redirect(`/admin/error?error="Invalid teams assignment!"`);
    }
    return response.redirect("/admin/events");
  } catch (error) {
    console.log(error);
    return response.redirect(`/admin/error?error=${error}`);
  }
});

// EDIT AND DELETE TEAM
app.post("/admin/api/editTeam", (request, response) => {
  try {
    // console.log();request.body.strTeamShort
    if (request.body.submitType === "EDIT") {
      admindb.query(
        "UPDATE teams SET strTeam = ?, strTeamShort = ?, intFormedYear = ? , strDescriptionEN = ?, strTeamBadge = ? WHERE teamId = ?",
        [
          removeTags(request.body.strTeam),
          removeTags(request.body.strTeamShort).toUpperCase(),
          request.body.intFormedYear,
          removeTags(request.body.strDescriptionEN),
          request.body.strTeamBadge,
          request.body.teamId,
        ],
        (error, result, fields) => {
          // console.log(resu);
          if (result) {
            console.log("TEAM UPDATED");
          } else {
            console.log(error);
          }
        }
      );
    }
    if (request.body.submitType === "DELETE") {
      console.log("DELETING EVENT");
      // FIRST DELETE ALL THE EVENTS AND PLAYERS ASSOCIATED WITH THE TEAM ID
      console.log(request.body.teamId);
      admindb.query(
        "DELETE FROM events WHERE homeId = ? OR awayId = ?",
        [request.body.teamId, request.body.teamId],
        (error, result, fields) => {
          if (result) {
            console.log("Successfully Deleted all events with teamId: " + request.body.teamId);
          } else {
            console.log("Delete Unsuccessful for events with teamId: " + request.body.teamId);
          }
        }
      );
      admindb.query("DELETE FROM players WHERE players.teamId = ?", [request.body.teamId], (error, result, fields) => {
        if (result) {
          console.log("Successfully Deleted all players with teamId:" + request.body.teamId);
        } else {
          console.log("Successfully Deleted all players with teamId:" + request.body.teamId);
        }
      });
      // THEN DELETE THE TEAM ITSELF
      admindb.query("DELETE FROM teams WHERE teamId = ?", [request.body.teamId], (error, result, fields) => {
        if (result) {
          // Check image if exissts
          if (fs.existsSync(`./uploads/${request.body.strTeamShort}-${request.body.teamId}.jpeg`)) {
            fs.unlinkSync(`./uploads/${request.body.strTeamShort}-${request.body.teamId}.jpeg`);
          }
          console.log(`Successfully Deleted ID: ${request.body.teamId}`);
        } else {
          console.log(error);
        }
      });
    }
    return response.redirect("/admin/teams");
  } catch (error) {
    console.log(error);
    return response.redirect(`/admin/error?error=${error}`);
  }
});

// ADD TEAM
app.post("/admin/api/addTeam", async (request, response) => {
  try {
    const file = request.files.image;
    const teamObj = ({ strTeam, strTeamShort, strDescriptionEN, intFormedYear } = request.body);
    var newHighestTeamId = await getHighestId("teams", "teamId").then((result) => result[0].highest + 1);
    if (isNaN(newHighestTeamId)) {
      newHighestTeamId = 0;
    }
    console.log(newHighestTeamId);
    const fileName = `${request.body.strTeamShort.toUpperCase()}-${newHighestTeamId}`;
    const webSource = `http://localhost:8001/api/getImage?name=${fileName}`; // NEEDED FOR strTeamImage
    console.log(teamObj);
    console.log(newHighestTeamId);
    console.log(webSource);
    if (fileName && file) {
      console.log("CREATING FILE TO SERVER...");
      // ADD NEW FILE UPPLOAD
      file.mv(`uploads/${fileName}.jpeg`, (error) => {
        if (error) {
          console.log("AN Internal Sever error had occured");
          return response.status(500);
        } else {
          console.log("FILE UPLOADED");
          return response.status(200);
        }
      });
      console.log("ADDING TO THE DATABASE");
      // ADD NEW TEAM
      admindb.query(
        "INSERT INTO teams (teamId, strTeam, strTeamShort, intFormedYear, strDescriptionEN, strTeamBadge) VALUES (?,?,?,?,?,?)",
        [
          newHighestTeamId,
          removeTags(teamObj.strTeam.toUpperCase()),
          teamObj.strTeamShort,
          teamObj.intFormedYear,
          removeTags(teamObj.strDescriptionEN),
          webSource,
        ],
        (error, result, fields) => {
          if (result) {
            console.log("ADD TEAM SUCCESS");
          } else {
            console.log("ADD TEAM FAILED");
            console.log(error);
          }
        }
      );
    } else {
      console.log("INVALID INPUT");
    }
    return response.redirect("/admin/teams");
  } catch (error) {
    console.log(error);
    return response.redirect(`/admin/error?error=${error}`);
  }
});

// EDIT DESCRIPTION
app.post("/admin/api/editDescription", (request, response) => {
  try {
    console.log("EDITTING DESCRIPTION...");
    admindb.query(
      "UPDATE description SET strDescriptionEN = ?, strWebsite = ?, strBadge = ?  WHERE descId = 0",
      [
        removeTags(request.body.strDescriptionEN),
        removeTags(request.body.strWebsite),
        removeTags(request.body.strBadge),
      ],
      (error, result, fields) => {
        if (result) {
          console.log("EDITTED Description!");
        } else {
          console.log("Something is Wrong on your INPUT!");
          console.log(error);
        }
      }
    );
    return response.redirect("/admin/description");
  } catch (error) {
    console.log(error);
    return response.redirect(`/admin/error?error=${error}`);
  }
});

/* -------------- END OF ADMIN COMMANDS -------------------------*/

/* --------------------- START OF PUBLIC FETCH API --------------------- */
/**
 * Returns the nba description object
 */
app.get("/api/nba-description", (request, response) => {
  admindb.query("SELECT * FROM description", (error, result, fields) => {
    return response.json(result[0]);
  });
});

/**
 * Returns the list of teams object
 */
app.get("/api/teams", (request, response) => {
  admindb.query("SELECT * FROM teams", (error, result, fields) => {
    // console.log(result.length);
    var teams = [];
    for (let i = 0; i < result.length; i++) {
      teams.push(result[i]);
    }
    return response.json(teams).end();
  });
});

/**
 * Returns the list the object of events in a seasons
 */
app.get("/api/seasons", (request, response) => {
  // If a query is given
  if (Object.entries(request.query).length != 0) {
    admindb.query(
      `SELECT season, intHomeScore, intAwayScore, dateEvent, strVideo, homeTeam.strTeam as strHomeTeam, awayTeam.strTeam as strAwayTeam, CONCAT(homeTeam.strTeam, ' vs. ' ,awayTeam.strTeam) as strEvent FROM events  INNER JOIN teams homeTeam ON events.homeId = homeTeam.teamId INNER JOIN teams awayTeam ON events.awayId = awayTeam.teamId  WHERE season = "${request.query.season}"`,
      (error, result, fields) => {
        return response.json(result);
      }
    );
  }
});

/**
 * Returns the list of seasons based on the events
 */
app.get("/api/seasons-list", (request, response) => {
  admindb.query("SELECT * FROM events", (error, result, fields) => {
    const seasons = [];
    for (let i = 0; i < result.length; i++) {
      seasons.push(result[i].season);
    }
    const s = seasons.filter((season, index) => seasons.indexOf(season) === index).sort();
    return response.json(s);
  });
});

/**
 * Returns the 15 latest events based on the date
 */
app.get("/api/latest-events", (request, response) => {
  admindb.query(
    "SELECT season, intHomeScore, intAwayScore, dateEvent, strVideo, homeTeam.strTeam as strHomeTeam, awayTeam.strTeam as strAwayTeam, CONCAT(homeTeam.strTeam, ' vs. ' ,awayTeam.strTeam) as strEvent FROM events INNER JOIN teams homeTeam ON events.homeId = homeTeam.teamId INNER JOIN teams awayTeam ON events.awayId = awayTeam.teamId   ORDER BY dateEvent DESC LIMIT 15",
    (error, result, fields) => {
      return response.json(result);
    }
  );
});

/**
 * Returns the list players object
 */
app.get("/api/players", (request, response) => {
  // console.log(request.query);
  if (request.query.search) {
    admindb.query(
      `SELECT strPlayerName, intHeight, intWeight, strTeam, strTeam FROM players INNER JOIN teams ON teams.teamId = players.teamId WHERE strPlayerName LIKE "%${
        request.query.search
      }%" LIMIT ${request.query.per_page} OFFSET ${request.query.page * request.query.per_page}`,
      async (error, result, fields) => {
        var totalPlayers = 0;
        totalPlayers = await getTotalPlayerSearch("players", request.query.search);
        console.log(totalPlayers);
        var jsonObject = {
          data: result,
          meta: {
            current_page: parseInt(request.query.page),
            per_page: parseInt(request.query.per_page),
            total_pages: parseInt(totalPlayers / parseInt(request.query.per_page) + 1),
            total_count: totalPlayers,
          },
        };
        return response.json(jsonObject);
      }
    );
  } else if (request.query.per_page && request.query.page) {
    admindb.query(
      `SELECT strPlayerName, intHeight, intWeight, strTeam, strTeam FROM players INNER JOIN teams ON teams.teamId = players.teamId LIMIT ${
        request.query.per_page
      } OFFSET ${request.query.page * request.query.per_page}`,
      async (error, result, fields) => {
        var totalPlayers = 0;
        totalPlayers = await getTotalRows("players");
        var data = [];
        for (let i = 0; i < result.length; i++) {
          const player = result[i];
          data.push(player);
        }
        var meta = {
          current_page: parseInt(request.query.page),
          per_page: parseInt(request.query.per_page),
          total_pages: parseInt(totalPlayers / parseInt(request.query.per_page) + 1),
          total_count: totalPlayers,
        };

        return response.json({ data, meta });
      }
    );
  }
});
/* --------------------- END OF PUBLIC FETCH API --------------------- */

/* ------------- IMAGE PROCESS ---------------------*/
app.post("/admin/api/uploadimage", (request, response) => {
  let file = request.files.image;
  let fileName = request.query.name;
  console.log(fileName);
  console.log(file);
  file.mv(`uploads/${fileName}.jpeg`, (error) => {
    if (error) {
      console.log("AN Internal Sever error had occured");
      return response.status(500);
    } else {
      console.log("FILE UPLOADED");
      return response.status(200);
    }
  });
  return response.redirect("/admin/teams");
});

app.get("/api/getImage", (request, response) => {
  let fileName = `${request.query.name}.jpeg`;

  response.sendFile(`uploads/${fileName}`, { root: __dirname }, (err) => {
    response.status(404).end();
  });
});

/* ------------------ HELPER FUNCTIONS ---------------------------- */
const getTotalRows = (tableName) => {
  return new Promise((resolve, reject) => {
    admindb.query(`SELECT COUNT(*) as count FROM ${tableName}`, (err, result, fields) => {
      if (err) {
        return reject(err);
      } else {
        resolve(result[0].count);
      }
    });
  });
};

const getTotalPlayerSearch = (tableName, search) => {
  return new Promise((resolve, reject) => {
    admindb.query(
      `SELECT COUNT(*) as count FROM ${tableName} WHERE strPlayerName LIKE "%${search}%" `,
      (err, result, fields) => {
        if (err) {
          return reject(err);
        } else {
          resolve(result[0].count);
        }
      }
    );
  });
};

const getTeamList = () => {
  return new Promise((resolve, reject) => {
    admindb.query(`SELECT teamId, strTeam FROM teams`, (error, result, fields) => {
      if (error) {
        return reject(error);
      } else {
        resolve(result);
      }
    });
  });
};

const getHighestId = (tableName, idName) => {
  return new Promise((resolve, reject) => {
    admindb.query(`SELECT MAX(${idName}) as highest FROM ${tableName}`, (error, result, fields) => {
      if (error) {
        return reject(error);
      } else {
        resolve(result);
      }
    });
  });
};

const getSeasonString = (date) => {
  console.log("CONSTRUCTING SEASON STRING");
  console.log(typeof date);
  const year = parseInt(date.split("-")[0]);
  var season = "";
  if (year % 2 === 0) {
    season = `${year - 1}-${year}`;
  } else {
    season = `${year}-${year - 1}`;
  }
  console.log(`The Season = ${season}`);
  return season;
};

const checkSession = (request) => {
  return request.session.admin;
};

const removeTags = (string) => {
  return string.replace(/(<([^>]+)>)/gi, "");
};
/* ------------------ HELPER FUNCTIONS ---------------------------- */
