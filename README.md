# Quick Access

- [sportsdb](https://www.thesportsdb.com/api.php)
- [balldontlie](https://www.balldontlie.io/)
- [website](https://ba-v-ab.netlify.app/)
- [database link](https://drive.google.com/drive/folders/1Y92TogEeUzae0s257p5Zk-XGOu1eGqLX?usp=sharing)

## HOW TO SETUP SERVER

NOTE: PLEASE IMPORT THE DATABASE NAMED bavab.sql with name bavab in your wamp server in order for the application to have the right data sets

1. Make sure you have that the database is running at <http://localhost> using WAMP service
2. To start the server, then enter **node install**, then enter **node app.js**, this will start your local server, be sure that the server is running at all times.
3. For Public page, Connect through <http://localhost:8001/>
4. For Admin page, Connect through <http://localhost:8001/admin>
5. Admin Page [username: admin, password: admin123]

## OUR PROJECT UTILIZES THE FOLLOWING SERVICES

- [express](https://www.npmjs.com/package/express) for REST API structure
- [mysql](https://www.npmjs.com/package/mysql) for connection to the database and using mysql queries
- [nodemon](https://www.npmjs.com/package/nodemon) for debugging/auto-refresh of application on testing the server
- [express-session](https://www.npmjs.com/package/express-session) for storing session data for authentication of the user
- [body-parse](https://www.npmjs.com/package/body-parse) for parsing json objects
- [express-fileupload](https://www.npmjs.com/package/express-fileupload) for parsing large files
- [pug](https://www.npmjs.com/package/pug) for rendering the application using pug with passed variables from node JS

- [jquery](https://jquery.com/) for better impementation for the functionality of the design for admin module
- [bootstrap](https://getbootstrap.com/) for CSS class application for the design of the admin module
- [pug](https://pugjs.org/api/getting-started.html) for more effieicient application of the html structure for admin module

## OUR API LINKS

### Public Page

- Public Home Page: <http://localhost:8001/>
- Seasons Page: <http://localhost:8001/seasons>
- Teams Page: <http://localhost:8001/teams>
- Players Page: <http://localhost:8001/players>

### Admin Module Page

- Admin Login Screen: <http://localhost:8001/admin>
- Admin Homepage: <http://localhost:8001/admin/home>
- Admin Homepage: <http://localhost:8001/admin/description>
- Admin Homepage: <http://localhost:8001/admin/events>
- Admin Homepage: <http://localhost:8001/admin/teams>
- Admin Homepage: <http://localhost:8001/admin/players>

### Public API

- **/api/nba-description** (GET) Returns the description of NBA from the database
- **/api/teams** (GET) Returns the list of teams from the database
- **/api/seasons** (GET) Returns the list of events from the database
- **/api/season-list** (GET) Returns the list of available seasons based on the events from the database
- **/api/latest-events** (GET) Returns 15 events ordered by date from the events table
- **/api/players** (GET) Returns the list of players from the database
- **/api/getImage** (GET) Returns the image file from the server

### Admin API

- **/admin/api/login** (POST) Verifies the user from the database and save the session of the user if login is successful
- **/admin/api/logout** (GET) Removes the session of the user from the browser
- **/admin/api/editPlayer** (POST) Edits/Deletes the player depending on the given object
- **/admin/api/editEvents** (POST) Edits/Deletes the events depending on the given object
- **/admin/api/editTeams** (POST) Edits/Deletes the Teams depending on the given object, also deletes the players and events related with the selected team
- **/admin/api/editDescription** (POST) Edits the description from the database
- **/admin/api/addPlayer** (POST) Adds a new player to the database
- **/admin/api/addEvent** (POST) Adds a new event to the database
- **/admin/api/addTeam** (POST) Adds a new team to the database

## API LINKS USED PREVIOUSLY AS REFERENCE (examples)

- Main API Page: <https://www.thesportsdb.com/api.php>
- League Description: <https://www.thesportsdb.com/api/v1/json/1/lookupleague.php?id=4387>
- Single Event: <https://www.thesportsdb.com/api/v1/json/1/lookupevent.php?id=1057679>
- Seasons List: <https://www.thesportsdb.com/api/v1/json/1/search_all_seasons.php?id=4387>
- Teams List: <https://www.thesportsdb.com/api/v1/json/1/lookup_all_teams.php?id=4387>
- Season Events (up to 100 only): <https://www.thesportsdb.com/api/v1/json/1/eventsseason.php?id=4387&s=2019-2020>
- Last 15 Events by League ID: <https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4387>

- Secondary API Page: <https://www.balldontlie.io/#getting-started>
- All players from all seasons: <https://www.balldontlie.io/api/v1/players?page=1&per_page=100&search=james>
- Retrieves all players from all seasons: <https://www.balldontlie.io/api/v1/stats?page=1&per_page=100&seasons[]=2018&player_ids[]=237>

## Main Requirement

Consider your previous application on accessing data from web service APIs. You will modify
the application so that it will include server-side functionality.

- The application will consist of two modules:

- **Public Website**

  - This is a modified version of the original website you created for the midterms.
    The main modification is that the source of the data that you will display will
    come from a database instead of a web service.
  - Some information presented may change depending on your database
    structure (to be discussed further below)

- **Admin Module**

  - This will update the database with the information that you need to display in
    your website. (This is as if you are the ones maintaining the data provided by
    the service).
  - Provide the appropriate add and view features for the data that you store in
    the database. The user must log in to use this module. Hence, session-
    handling mechanisms should be integrated accordingly.
  - Provide basic data validations.

- Simplify your database design in order to facilitate the development of the admin
  module.
- In addition, ensure that some of your HTML is generated from the server (for both
  modules), instead of generating it from JavaScript.
- Use either PHP or Node.js (with Express) to create your server-side scripts. Only front-end
  CSS libraries or frameworks are allowed. References to JavaScript libraries are allowed
  as long as these are for design purposes only.
- All project code (HTML, CSS, and scripts, submitted by the group must represent
  ORIGINAL WORK by the members of the group. All functional codebase sections must
  be tagged (through comments embedded in the code) with the group member(s)
  responsible for the code, as well as with concise descriptions of the code section's
  purpose.

## Initial Task

Create a database that contains the following:

- a table for user accounts: this is intended for user validation
- data table/s whose structure accommodates the data similar to those in your web
  services; this data is what you intend to be accessed from your website’s public
  pages

  - these tables may include blob types for images you may want to store in your
    database or alternatively, relative URL’s for the image files stored in the server
  - your data may include links to external websites for videos or other 3rd-party
    information
  - column names must be descriptive to facilitate understanding of your
    schema

## Other Task

1. Document your output in the following manner:
   1. Create an inventory of your web resources (each file in your application folder
      along with a description of its purpose and/or contents, and the page(s) and/or
      UI(s) that utilize it). Collection of similar resources (e.g., stylesheets, images) may be
      described collectively. Include supporting information as to what module(s) / UI(s) /
      page(s) access or utilize this resource. Use a 2- column table to present this information.
   2. Relational Database Schema
   3. Provide a list of essential features with a description of the UI's implemented functionality
      and user interaction if any. Use a 2-column table to present this information.
   4. Include a cover page that shows the list (in alphabetical order) of team members who will be
      given credit for the activity.
2. Prepare a video presentation (15 minutes max) of your project, similar to how you
   presented for the midterms. (due on Monday, 14 December)
   1. application demo
   2. code:
      1. server-side processing and interaction between client and server
      2. ensure that your codes are large enough to see
      3. you may use powerpoint slides for some parts of the presentation
3. Deployment demo: to be done on Saturday, 12 December in the virtual lab
   1. each team will deploy their application and set up their server using a virtual host
   2. the team should be able to show that the public website and the admin module
      can run in a separate terminal via the appropriate URL on the browser.
4. Peer evaluation: (due on Monday, 14 December)
   1. follow the same instructions given for the midterms
