/**
 * seasons.js
 * javascript file for implementing DOM and retrieving fetch request for seasons page
 */

/**
 * Global variable for array of all teams {teamName: "", img: ""}
 */
var teamsArray = [];

/**
 * return the src image of the given teamName
 * used to get the image of a team on the list of events in seaons
 */
getImageByTeam = (teamName) => teamsArray.filter((team) => team.teamName === teamName)[0].teamImage;

/**
 * onload method to be used on seasons.html
 * this function loads automatically every time seasons.html is returned
 */
const load = async () => {
  // Load the object of teams, and set aside the teams to an array
  console.log("loading...");
  document.getElementById("loading").style.display = "flex";
  // document.getElementById("seasons-header").style.display = "none";
  const teams = await fetchTeams();
  for (team of teams) {
    teamsArray.push({ teamName: team.strTeam, teamImage: team.strTeamBadge });
  }
  console.log(teams);
  document.getElementById("loading").style.display = "none";
  document.getElementById("seasons-header").style.display = "block";
  console.log("loading Ends.");

  //Get the parameter of the url
  const params = new URLSearchParams(window.location.search);
  const currentSeasonYear = params.get("y");
  document.getElementById("seasons-header").innerHTML = `Game Season on Year ${params.get("y")}`;

  // Search list select option
  searchList();

  // Update the lists of events in a seasons
  updateSeasonCards(currentSeasonYear);
};

/**
 * Update the seasons object to be fetched and update the data to be rendered
 * Resets and retrieve the content for the page
 */
const updateSeasonCards = async (seasonYear, filterTeam) => {
  // reset card container
  document.getElementById("card-seasons-container").innerHTML = "";

  // Get all the events to be assgined as object
  console.log("loading...");
  document.getElementById("loading").style.display = "flex";
  document.getElementById("seasons-header").style.display = "none";

  events = await fetchSeasonEvents(seasonYear);

  //Setup initial message
  let resultMessage = `Total Events: <b>${events.length}</b>`;

  // efines if the team name is given
  if (filterTeam === undefined || filterTeam === "Show All") {
    console.log("No team given.");
  } else {
    events = events.filter((event) => event.strAwayTeam === filterTeam || event.strHomeTeam === filterTeam);
    resultMessage = `Total Events: <b>${events.length}</b> for team of <b>${filterTeam}</b>`;
  }

  console.log(events);
  console.log("loading Ends.");
  document.getElementById("loading").style.display = "none";
  document.getElementById("seasons-header").style.display = "block";

  // message List result
  document.getElementById("list-results").innerHTML = resultMessage;

  //get the pagination-cont
  const divPagination = document.getElementById("pagination-cont");

  // limit of set of data available per page
  const cardLimit = 6;

  // prevent select page tag to duplicate when clicking new season
  addSeasonCard(0, events.length < cardLimit ? events.length : cardLimit, events);

  // remove content of the pagination-cont to prevent duplicates
  while (divPagination.childNodes.length > 0) {
    divPagination.removeChild(divPagination.lastChild);
  }

  // select tag is the current navigation option
  const pageLabel = document.createElement("h2");
  const dropdown = document.createElement("select");

  // Add options in the select tag
  for (let i = 0; i < events.length / cardLimit; i++) {
    let pageButton = document.createElement("option");
    pageButton.innerHTML = i + 1;
    pageButton.value = i;
    dropdown.append(pageButton);
  }
  pageLabel.innerHTML = "Page";

  //This addeventListener is used instead of "pageButton" in for loop due to compatibility issues
  dropdown.addEventListener("change", (event) => {
    // reset card container
    document.getElementById("card-seasons-container").innerHTML = "";

    // if events rows does not exceed to cardLimit(6), else add change pageindex from 0 to greater than 0
    if (events.length < cardLimit) {
      addSeasonCard(0, events.length, events);
    } else {
      let pageIndex = event.target.value;
      pageIndex = parseInt(pageIndex);
      addSeasonCard(pageIndex * cardLimit, (pageIndex + 1) * cardLimit, events);
    }
  });

  // Append  the dropdown select element with proper values
  divPagination.append(pageLabel, dropdown);
};

/**
 * Add a card for the list of seasons, used for card generation based on the
 *  min index and max index are needed for the generation of the current state of
 *  cards in the list
 */
const addSeasonCard = async (minIndex, maxIndex, events) => {
  try {
    for (let i = minIndex; i < maxIndex; i++) {
      //Setup card
      const card = document.createElement("div");
      card.className = "card-season";

      // Create elements
      const spanTitle = document.createElement("h3");
      const spanDate = document.createElement("h4");

      const tableGame = document.createElement("table");
      const rowHome = tableGame.insertRow(0);
      const rowAway = tableGame.insertRow(1);
      const imgHome = document.createElement("img");
      const imgAway = document.createElement("img");

      const anchor = document.createElement("a");

      // default height and width of images can be addded to css
      imgHome.width = 64;
      imgHome.height = 64;
      imgAway.width = 64;
      imgAway.height = 64;

      // Add content to the elements
      imgHome.src = getImageByTeam(events[i].strHomeTeam);
      imgAway.src = getImageByTeam(events[i].strAwayTeam);
      spanTitle.innerHTML = events[i].strEvent;
      spanDate.innerHTML = events[i].dateEvent;
      rowHome.insertCell(0).appendChild(imgHome);
      rowHome.insertCell(1).innerHTML = events[i].strHomeTeam;
      rowHome.insertCell(2).innerHTML = events[i].intHomeScore;
      rowAway.insertCell(0).appendChild(imgAway);
      rowAway.insertCell(1).innerHTML = events[i].strAwayTeam;
      rowAway.insertCell(2).innerHTML = events[i].intAwayScore;
      anchor.href = events[i].strVideo;
      anchor.innerHTML = events[i].strVideo == null || events[i].strVideo == "" ? "" : "Watch Highlights";

      // Append elements to the parent div
      card.append(spanTitle, spanDate, tableGame, anchor);
      document.getElementById("card-seasons-container").appendChild(card);
    }
  } catch {
    //Catch is needed because an error occurs whenever the
    // number of results is not divisible to the max index
  }
};

/**
 * Contains the functionalities on the team list filter to be
 * created in the seasons page
 */
const searchList = () => {
  // setup elements needed and element to be appended
  const searchContainer = document.getElementById("search-cont");
  const h2 = document.createElement("h2");
  const selectTeam = document.createElement("select");
  const resetButton = document.createElement("button");

  //Get the parameter of the url
  const params = new URLSearchParams(window.location.search);
  const currentSeasonYear = params.get("y");

  h2.innerHTML = "Team Filter";
  resetButton.innerHTML = "reset";

  // reset button resets the list of cards
  resetButton.addEventListener("click", () => {
    updateSeasonCards(currentSeasonYear);
  });

  // First option as show all
  let firstOption = document.createElement("option");
  firstOption.innerHTML = "Show All";
  selectTeam.appendChild(firstOption);

  // add options to select
  for (team of teamsArray) {
    let teamName = team.teamName;
    const option = document.createElement("option");
    option.innerHTML = teamName;
    selectTeam.appendChild(option);
  }

  // add event listener on click of an option to change the list of cards
  selectTeam.addEventListener("change", (event) => {
    updateSeasonCards(currentSeasonYear, event.target.value);
  });

  searchContainer.append(h2, selectTeam, resetButton);
};
