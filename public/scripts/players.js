/**
 * players.js
 * javascript file for implementing DOM and retrieving fetch request  for players page
 */

/**
 * Variables needed for the current state of the page of the table
 */
var currentPage = 0;
var pageLimit = 0;
var maxPage = 0;
var totalCount = 0;

/**
 * onload method to be used on players.html
 * this fucntion loads automatically everytime players.html is returned
 */
const load = async () => {
  //Get the object meta based on the response from api, load the array meta
  console.log("loading...");
  document.getElementById("loading").style.display = "block";
  // document.getElementById("player-form").style.display = "none";
  // document.getElementById("page-buttons").style.display = "none";
  const arrayMeta = await fetchPlayerList(0, 70).then((object) => object.meta);
  console.log(arrayMeta);
  document.getElementById("loading").style.display = "none";
  document.getElementById("player-form").style.display = "flex";
  document.getElementById("page-buttons").style.display = "block";
  console.log("loading ends.");

  // setup variables
  currentPage = arrayMeta.current_page;
  pageLimit = arrayMeta.per_page;
  maxPage = arrayMeta.total_pages;
  totalCount = arrayMeta.total_count;

  // Set the players result to the total count of players based on the api
  document.getElementById("player-search-message").innerHTML = `Total Number of players: (${totalCount})`;

  //generate a select dropdown tag with numbers and working values on change
  generateSelectDropdown(pageLimit, maxPage);

  //display the table once the page loads
  updatePlayerTable();

  // Search Player on press enter
  document.getElementById("player-search").addEventListener("keyup", (event) => {
    // when enter is pressed
    if (event.key === "Enter") {
      searchPlayer();
    }
  });

  // Add function to next anchor tag
  document.getElementById("next-players").addEventListener("click", () => {
    nextPlayers();
  });

  // Add function to previous anchor tag
  document.getElementById("prev-players").addEventListener("click", () => {
    prevPlayers();
  });
};

/**
 * Generate select tag based on the page limit
 */
const generateSelectDropdown = (pageLimit, maxPage) => {
  var divSelect = document.getElementById("players-select");
  var selectPlayers = document.createElement("select");
  divSelect.appendChild(selectPlayers);
  selectPlayers.id = "players-list-select";

  // Generate options on the select tag
  for (var i = 1; i <= maxPage; i++) {
    var optionPlayer = document.createElement("option");
    optionPlayer.innerHTML = i; //display
    optionPlayer.value = i; //true value
    selectPlayers.appendChild(optionPlayer);
  }

  // event listener to update button when value changes on dropdown
  selectPlayers.addEventListener("change", (event) => {
    currentPage = event.target.value;
    updatePlayerTable();
  });
};

/**
 * Update the table generated based on the page given
 */
const updatePlayerTable = async () => {
  //display buttons on update
  document.getElementById("page-buttons").style.display = "block";
  const aNext = document.getElementById("next-players");
  const aPrev = document.getElementById("prev-players");
  aPrev.style.display = currentPage <= 1 ? "none" : "block";
  aNext.style.display = currentPage >= maxPage ? "none" : "block";

  var table = document.getElementById("player-table");

  // fetch and loading
  console.log("loading...");
  document.getElementById("loading").style.display = "block";
  const playersData = await fetchPlayerList(currentPage, pageLimit);
  const playerList = playersData.data;
  console.log(playerList);
  document.getElementById("loading").style.display = "none";
  console.log("end of loading.");

  document.getElementById("player-page-form").style.display = "flex";

  table.innerHTML = ""; //reset table whenever this method is called to prevent unwanted additional rows
  updatePlayerTableData(table, playerList);
};

/**
 * Changes added when the search button is clicked
 */
const searchPlayerTable = (playersList) => {
  players = playersList.data;
  var table = document.getElementById("player-table");
  table.innerHTML = ""; //reset table whenever this method is called to prevent unwanted additional rows

  document.getElementById("player-page-form").style.display = "none";
  document.getElementById("page-buttons").style.display = "none";

  if (players.length === 0 || players.length >= 100) {
    let h3 = document.createElement("h3");
    if (players.length === 0) {
      h3.innerHTML = "Player Not Available";
    } else if (players.length >= 100) {
      h3.innerHTML = "Search key too broad.";
    }
    table.style.textAlign = "center";
    table.appendChild(h3);
  } else {
    updatePlayerTableData(table, players);
  }
};

/**
 * Display the table based on the players object given
 *  and based on the table to be updated
 */
const updatePlayerTableData = (table, players) => {
  // Construct Headers for table
  let headers = ["Name", "Weight", "Height(cm.)", "Team"];
  let thead = table.createTHead();
  let row = thead.insertRow(0);

  // Populate row with table th
  for (let i = 0; i < headers.length; i++) {
    var th = document.createElement("th");
    th.innerHTML = headers[i];
    row.insertCell(i).append(th);
  }

  // Construct the body of the table
  let tbody = table.createTBody();
  //Generate rows for the table
  for (player of players) {
    let row = tbody.insertRow(-1);
    row.insertCell(0).innerHTML = `${player.strPlayerName}`;
    row.insertCell(1).innerHTML = player.intWeight == "" ? "N/A" : parseInt(player.intWeight);
    row.insertCell(2).innerHTML = player.intHeight == undefined ? "N/A" : player.intHeight;
    row.insertCell(3).innerHTML = player.strTeam;
  }
};

/*
 * Function for searching the players based on the input of the user
 */
const searchPlayer = async () => {
  // Value of the input
  const searchKey = await document.getElementById("player-search").value;

  // Get the data based on the given search query
  document.getElementById("loading").style.display = "block";
  const players = await fetchPlayerSearch(0, 100, searchKey);
  document.getElementById("loading").style.display = "none";

  // Prompt the user about the search results
  document.getElementById(
    "player-search-message"
  ).innerHTML = `Searching <b>${searchKey}</b>, results (${players.meta.total_count})`;

  // Prompt the user that the search key cannot be too broad
  if (players.meta.total_count > 100) {
    document.getElementById("player-search-message").innerHTML =
      "Player search too broad, please type more specific keys";
  }

  // update the table based on the search given new set of objects
  searchPlayerTable(players);
};

/**
 * Reset the table of players to default
 */
const resetPlayers = () => {
  // Reset current value of search input
  document.getElementById("player-search").value = "";
  // Reset the table
  updatePlayerTable(currentPage, pageLimit);
  // Reset the search message
  document.getElementById("player-search-message").innerHTML = `Total Number of players: (${totalCount})`;
};

/**
 * go to  Next Page of the current page of the players
 */
const nextPlayers = () => {
  // If the current page reaches the limit, do not apply the function
  if (currentPage >= maxPage) {
    console.log("Max page reached");
  } else {
    var select = document.getElementById("players-list-select");
    select.value = parseInt(select.value) + 1;
    currentPage = parseInt(currentPage) + 1;
    document.getElementById("player-table").scrollIntoView();
    updatePlayerTable(currentPage, pageLimit);
  }
};

/**
 * go to Previous Page of the current page of the players
 */
const prevPlayers = () => {
  // If the current page reaches the first page, do not apply the function
  if (currentPage <= 1) {
    console.log("Min page reached");
  } else {
    var select = document.getElementById("players-list-select");
    select.value = parseInt(select.value) - 1;
    currentPage = parseInt(currentPage) - 1;
    document.getElementById("player-table").scrollIntoView();
    updatePlayerTable(currentPage, pageLimit);
  }
};
