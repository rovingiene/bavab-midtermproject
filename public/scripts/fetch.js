/**
 * fetch.js
 * Contains all the fetch data of the object to be retrieved
 * Reference for now, this js file should be imported to other js files for less repeating functions
 */

//  Url Constants
const leagueID = 4387; // ID OF NBA League
const url = "https://www.thesportsdb.com/api/v1/json/1"; // URL of our primary API
const url2 = "https://www.balldontlie.io/api/v1"; // URL for the api of balldontlie.io

const url3 = "http://localhost:8001"; //localhost url

/**
 * return the object of NBA league USING OUR API
 */
const fetchLeague = () =>
  fetch(`${url3}/api/nba-description`)
    .then((response) => response.json())
    .then((response) => response)
    .catch((error) => error);

/**
 * Returns the array of the latest events in NBA LeagueUSING OUR API
 */
const fetchLatestEvents = () =>
  fetch(`${url3}/api/latest-events`)
    .then((response) => response.json())
    .then((response) => response)
    .catch((error) => error);

/**
 * Returns the array list of the years of the seasons in NBA League
 */
const fetchSeasonList = () =>
  fetch(`${url3}/api/seasons-list`)
    .then((response) => response.json())
    .then((response) => response)
    .catch((error) => error);

/**
 * Returns the array list of events based on the season year in NBA League
 */
const fetchSeasonEvents = (seasonYear) =>
  fetch(`${url3}/api/seasons?season=${seasonYear}`)
    .then((response) => response.json())
    .then((response) => response)
    .catch((error) => error);

/**
 * Returns the array of available list of teams in NBA League
 */
const fetchTeams = () =>
  fetch(`${url3}/api/teams`)
    .then((response) => response.json())
    .then((response) => response)
    .catch((error) => error);

/**
 * Returns two objects,
 *  1. NBA league object as data { data: Array[] , ...}
 *  2. Array meta { ... , meta: {total_pages: "", current_page: "", next_page: "", per_page: "", total_count: ""} }
 *
 * Note that balldontlie api contains JSON object that is different from thesportsdb,
 * basically, balldontlie requires pages to navigate the objects to return
 *  and thesportsdb returns all the objects without pagination
 */
const fetchPlayerList = (page, limit) =>
  fetch(`${url3}/api/players?page=${page}&per_page=${limit}`)
    .then((response) => response.json())
    .then((response) => ({ data: response.data, meta: response.meta }))
    .catch((error) => error);

/**
 * Return list of players based on the search value
 */
const fetchPlayerSearch = (page, limit, search) =>
  fetch(`${url3}/api/players?page=${page}&per_page=${limit}&search=${search}`)
    .then((response) => response.json())
    .then((response) => ({ data: response.data, meta: response.meta }))
    .catch((error) => error);
