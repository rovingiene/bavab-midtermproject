/**
 * global.js
 * javascript file implemented for all the html file
 */
var seasonLists = [];

/**
 * Retrieves and displays all the available seasons on header
 */
const getSeasonYearList = async () => {
  // Get all the list of seasons available for the header
  console.log("Loading seasons...");
  seasonList = await fetchSeasonList();
  console.log(seasonList);
  console.log("End of loading seasons..");

  // Loop the content of list of season
  for (seasonYear of seasonList) {
    // Create the anchor tag with the season year href and innerHTML
    const a = document.createElement("a");
    a.innerHTML = seasonYear;
    a.href = `./seasons?y=${seasonYear}`;

    //Append the list of seasons as anchor tag to Seasons anchor and add a break in between the creation of anchor tags
    document.getElementById("season-dropdown-content").append(a, document.createElement("br"));
  }
};

/**
 * Change the window url to the designated season year
 */
const goToSeasonYear = async (seasonYear) => {
  console.log(seasonYear);
  window.location.href = `./seasons.html?y=${seasonYear}`;
};
