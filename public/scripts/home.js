/**
 * home.js
 * javascript file for implementing DOM and retrieving fetch request  for homepage
 */

/**
 * Index of the slide show
 */
var slideIndex = 0;

/**
 * onload method to be used on home.html
 * this fucntion loads automatically everytime home.html is returned
 */
const load = async () => {
  // Load the data of the league (description and icon)
  console.log("loading...");
  document.getElementById("loading").style.display = "flex";
  const nbaLeague = await fetchLeague();
  console.log(nbaLeague);
  document.getElementById("loading").style.display = "none";
  console.log("loading ends.");

  //Divides the long description to sentence
  const paragraphs = paragraphList(nbaLeague.strDescriptionEN, 4);

  // This can be used for further information on description
  // for (let i = 1; i < paragraphs.length; i++) {
  //   let p = paragraphs[i];
  //   console.log(p);
  // }

  // Create elements
  const p = document.createElement("p");
  const img = document.createElement("img");

  // Setup properties of elements
  img.alt = "nba icon";
  img.width = 150;
  img.height = 140;

  // Add content to the elements
  p.innerHTML = paragraphs[0];
  img.src = nbaLeague.strBadge;

  // Append elements
  document.getElementById("league-description").append(img, p);

  //SlidesShow
  showSlides();

  // load the data of the latest events (15 events starting from the latest)
  console.log("loading...");
  document.getElementById("loading").style.display = "flex";
  const latestEvents = await fetchLatestEvents();
  console.log(latestEvents);
  document.getElementById("loading").style.display = "none";
  console.log("loading ends.");

  // Update the content of the table for the list of latest events
  updateEventsTable(latestEvents);
};

/**
 * returns the list of paragraphs to divide from a long string
 * for every number of periods given
 *
 * ex. a longText with 10 periods, when the numberOfPeriods is 2,
 *  the paragraphList will be divided in to 5, and returns a list
 *  for each of every two sentence
 */
const paragraphList = (longText, numberOfPeriods) => {
  var sentences = longText.split(". ");
  var paragraphs = [];
  var p = "";
  // Combines the sentences needed, lets say a paragraph is formed for every 4 sentence
  for (let i = 0; i < sentences.length; i++) {
    p += `${sentences[i]}. `;
    if (i % numberOfPeriods === 0 && i >= numberOfPeriods) {
      paragraphs.push(p);
      p = "";
    }
  }
  return paragraphs;
};

/**
 * Update the events table by providing the latest events data
 *  The table with id of latest-events-table is updated to have a content
 *  based on the data given.
 */
const updateEventsTable = (latestEvents) => {
  const table = document.getElementById("latest-events-table");

  // Generate headers for the table
  const headers = ["Game", "Home", "Score", "Visitor", "Score", "Date"];
  let thead = table.createTHead();
  let row = thead.insertRow(0);
  for (let i = 0; i < headers.length; i++) {
    var th = document.createElement("th");
    th.innerHTML = headers[i];
    row.insertCell(i).append(th);
  }
  let tbody = table.createTBody();

  // Generate rows and insert cells for each row based on the given object
  for (let i = 0; i < latestEvents.length; i++) {
    let row = tbody.insertRow(-1);
    row.insertCell(0).innerHTML = latestEvents[i].strEvent;
    row.insertCell(1).innerHTML = latestEvents[i].strHomeTeam;
    row.insertCell(2).innerHTML = latestEvents[i].intHomeScore;
    row.insertCell(3).innerHTML = latestEvents[i].strAwayTeam;
    row.insertCell(4).innerHTML = latestEvents[i].intAwayScore;
    row.insertCell(5).innerHTML = latestEvents[i].dateEvent;

    // Add link funcitonality when a row in the table is clicked
    row.addEventListener("click", (event) => {
      window.open(latestEvents[i].strVideo);
    });
  }

  // console.log(table.rows);
  // for (let i = 1; i < table.rows.length; i++) {
  //   console.log(table.rows[i]);
  //   table.rows[i].addEventListener("click", (event) => {
  //     console.log("row");
  //     console.log(i);
  //   });
  // }
};

/**
 * Manages the slide procedure in the homepage
 */
function showSlides() {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
  setTimeout(showSlides, 2000); // Change image every 2 seconds
}
