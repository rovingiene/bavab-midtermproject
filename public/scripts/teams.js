/**
 * players.js
 * javascript file for implementing DOM and retrieving fetch request for seasons page
 */

/**
 * Save the list of teams as a variable to the array
 */
var teamList = [];

/**
 * onload method to be used on teams.html
 * this fucntion loads automatically everytime teams.html is returned
 */
const load = async () => {
  // Load the data
  console.log("loading...");
  document.getElementById("loading").style.display = "block";
  // document.getElementById("search-container").style.display = "none";
  teamList = await fetchTeams();
  console.log(teamList);
  document.getElementById("loading").style.display = "none";
  document.getElementById("search-container").style.display = "flex";
  console.log("end of loading...");

  //populate team-list div with the object assigned from teamList
  generateTeamList(teamList);
};

/**
 * open team modal and update the modal content
 */
const openTeamModal = (team) => {
  // Target Modal
  let modal = document.getElementById("team-modal");
  modal.firstElementChild.innerHTML = ""; //remove to reset

  // Show Modal
  modal.style.display = "block";

  // Get the modal div
  const modalContent = modal.firstElementChild;

  // Create elementss
  const img = document.createElement("img");
  const h3 = document.createElement("h3");
  const p = document.createElement("p");
  const button = document.createElement("div");

  // add properties to the modal content
  button.id = "close-modal";
  button.className = "close-modal";
  button.innerHTML = "x";

  // update Modal Content
  img.src = team.strTeamBadge;
  h3.innerHTML = team.strTeam;
  p.innerHTML = team.strDescriptionEN;

  modalContent.append(button, img, h3, p);

  // add function to close the modal
  closeTeamModal();
};

/**
 * Functions added to close the modal
 */
const closeTeamModal = () => {
  let modal = document.getElementById("team-modal");
  let closeModal = document.getElementById("close-modal");

  // for close button
  closeModal.addEventListener("click", () => {
    // Hide modal and reset the modal
    modal.style.display = "none";
    modal.firstElementChild.innerHTML = "";
  });

  // When mouse pointer clicked outside the modal
  window.onclick = (event) => {
    if (event.target == modal) {
      // Hide modal and reset the modal
      modal.style.display = "none";
      modal.firstElementChild.innerHTML = "";
    }
  };
};

/**
 * Generate the cards with the object of teams as the content
 */
const generateTeamList = (teams) => {
  document.getElementById("team-list").innerHTML = "";

  for (team of teams) {
    // Create elements
    const teamDiv = document.createElement("div");
    const teamName = document.createElement("h3");
    const teamShortHand = document.createElement("p");
    const teamImage = document.createElement("img");

    // Add properties to image
    teamImage.width = 75;
    teamImage.height = 75;

    // Add content to  the  elements
    teamImage.src = team.strTeamBadge;
    teamName.innerHTML = team.strTeam;
    teamShortHand.innerHTML = team.strTeamShort;

    //append to parentdiv
    teamDiv.appendChild(teamImage);
    teamDiv.appendChild(teamName);
    teamDiv.appendChild(teamShortHand);

    // Add listener on click
    teamDiv.addEventListener("click", (event) => {
      // object to be changed depending on the object clicked
      var teamObject;

      // If div is clicked
      if (event.target.nodeName === "DIV")
        teamObject = teamList.filter((team) => team.strTeam === event.target.children[1].innerHTML).pop();

      // IF team name is clicked
      if (event.target.nodeName === "H3")
        teamObject = teamList.filter((team) => team.strTeam === event.target.innerHTML).pop();

      // IF SHORTHAND is clicked
      if (event.target.nodeName === "P")
        teamObject = teamList.filter((team) => team.strTeamShort === event.target.innerHTML).pop();

      // IF image is clicked
      if (event.target.nodeName === "IMG")
        teamObject = teamList.filter((team) => team.strTeamBadge === event.target.src).pop();

      // open the modal based on the clicked object and switch the scroll to the modal
      document.getElementById("team-modal").scrollIntoView();
      openTeamModal(teamObject);
    });

    // set properties
    teamDiv.className = "team-card";
    teamName.className = "team-name";
    teamShortHand.className = "team-shortname";
    teamImage.className = "team-icon";

    // Append content to the parent div
    document.getElementById("team-list").appendChild(teamDiv);
  }
};

/**
 * Filters the team based on the value of the search key on the input
 */
const filterTeam = () => {
  const input = document.getElementById("filter-team");
  var searchKey = input.value;

  // prompt user about the searched key
  document.getElementById("search-message").innerHTML =
    searchKey !== "" ? `Searching team with keyword: <b>${searchKey}<b>` : "";

  // teamlist to be retrieved which is filtered by the search key
  var newTeamList = teamList.filter((team) => replaceString(team.strTeam).indexOf(replaceString(searchKey)) > -1);

  generateTeamList(newTeamList);
};

/**
 * Replace the string to remove space and set to upper case for searching purposes
 */
const replaceString = (value) => value.replace(/\s/g, "").toUpperCase();
